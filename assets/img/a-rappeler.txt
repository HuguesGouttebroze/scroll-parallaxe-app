a-reappeler

	-> 1.
		-> Rachel Parmentier
 relation de 2e niveau
· 2e
🦋 Ressource Humaine 🦋
VENDREDI
Rachel Parmentier a envoyé le message suivant à 09:12
Voir le profil de RachelRachel Parmentier
Rachel Parmentier  09:12

Bonjour Hugues !

J’espère que vous allez bien. 
Est-ce que cela vous plairait 
de changer d’air pour cette rentrée ?

Un des nos clients a besoin de vos talents 
au poste Lead développeur Vue.js et PHP Laravel.

Mon client est une SAS créée en 2016, spécialisée 
dans la gestion des transports et les TMS. 
Ils travaillent avec tous types d’entreprises. 
Cette entreprise mise vraiment sur le bien 
être en proposant, du télétravail 
(en ce moment 1 jour par semaine au bureau 
obligatoire), des battledev, des journées hackathon… 
En arrivant vous serez tout de suite 
intégré avec une période d’accompagnement 
et de formation avec vos collègues sur les technos 
utilisées et les produits de la société pour vous 
familiariser avec tout ça sans deadline critique.

Ce poste est à pourvoir sur Bron (près de Lyon) 
dès que vous serez disponible.

Si vous êtes intéressé laissez moi votre numéro 
de téléphone que l’on puisse en discuter.

Dans l'attente de votre retour !

Bonne journée 😃

Rachel Parmentier
🦋 Ressource Humaine 🦋

-------------------------------------------------
-------------------------------------------------
---> 2eme & veut 1 cvMax-Arthur BAUX
 relation de 3e niveau
· 3e
IT Talent Acquisition
AUJOURD’HUI
Max-Arthur BAUX a envoyé le message suivant à 10:35
Voir le profil de Max-ArthurMax-Arthur BAUX
Max-Arthur BAUX  10:35

Bonjour Hugues,

Je travaille chez XperPool, une société de portage salarial 
appartenant au groupe Adservio.

Nous sommes une communauté solidaire et bienveillante portée 
par une culture digitale forte, en bref, de véritables 
Crafts(wo)men !

J'ai pris connaissance de votre parcours et j'ai été 
interpellé par votre expérience.
Nous recherchons des talents comme vous pour de nouvelles 
missions chez un de nos nombreux partenaires 
(Thales, GRDF, RATP, BNP Paribas, Disneyland,...).

Si ce challenge vous tente, n’hésitez pas à m'envoyer 
votre CV et me transmettre vos disponibilités afin 
que je vous parler plus précisément de cette opportunité !

Bien cordialement,

Max-Arthur BAUX
IT Talent Acquisition at Adservio
