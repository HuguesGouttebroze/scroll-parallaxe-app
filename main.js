import './style.css'
import javascriptLogo from './javascript.svg'
import { setupCounter } from './counter.js'

document.querySelector('#scrollContainer').innerHTML = `
  <div>
  <p>Alors que je relisais le repo de code du projet Froggit (1 lab & ses featuresn 1 blog, 
    1 sit doc. , 1 api, et tt un tat de features diverses ...), je suis tomber sur 
    une partie de la doc technique du sit documentaires, dev avec docusaurus, donc React, 
    donc ECMAScrpit 6+, donc du JavaScript ...</p>
    - l'objectif, qd on écrit du code, est de suivre les pratiques menant à un code propre.
    Je devais respecter tt un tas de décisions, sur le lint, l'indenation, 
    des choix de nommaves, et on m'a aussi poussé à choisir la simplicité, c'est d'une logique 
    implacable qd j'y repence aujourd'hui, si notre code est écrit ds un but de collaboraion, doit etre relu, expliké pr etre 
    compris le + facilemen, de ° choisir la simplisité permet d'allernplus loin ds l'imlp;émentation de 
    d'algo complexe.
    Je devais expliquee ...
    créer des sections
Docusaurus étant un Framework de la librairie React, on y retrouve la même logique.
L'affichage d'une page web est un ensemble de composants réutilisables, que l'on peut assembler.
Nous allons nous appuyer sur l'utilisation de ces Composants pour créer nos différentes sections d'affichage, notamment pour gérer le rendu visuel de la page d'accueil du site index.js.
Chaque section de pages va être définie dans un Composant enfant dans le dossier ./src/components/, puis importée dans un Composant parent dans le dossier ./src/pages/.
Un composant (Componant) contient: des données qui constituent un état (state), des comportements et un rendu (l'affichage), ces trois notions vont interagir entre elles.

Création du composant
Pour l'exemple, nous allons expliquer comment nous avons créer la section chargée d'afficher les fonctionnalités (Features) de Froggit.
Dans le dossier src/components/, nous avons créé un fichier React HomepageFeatures.js.
Nous avons choisi de nommer notre fichier HomepageFeatures, en PascalCase, car ce composent est destiné à être affiché, dans la Homepage (page d'Accueil) et doit afficher les Features (fonctionnalités) de Froggit.
Ce fichier est composé de 3 parties:

une pour gérer les données de notre section ;
une pour définir les comportements que nous attendons en utilisant différentes fonctions ;
une pour définir l'affichage. Nous utilisons le JSX pour cette partie, voir l' introduction au JSX sur la doc de React pour des explications sur cette extension syntaxique du JavaScript.


Les données de notre section
Nous allons créer un tableau d'objets JavaScript pour définir les données à afficher dans notre section. Il y a 3 objets pour chaque élément du tableau :

Un titre, de type chaîne de charactère (String), donc entouré de guillements simples ' ou doubles ").
Une description, que nous définissons en tant qu' Element JSX. Nous pouvons ainsi lui donner un template de rendu comme valeur, avec les balises HTML de notre choix, juste un paragraphe dans notre cas. Pour cela, nous devons définir sa valeur entre des parenthèses et à l'intérieur d' une balise parent, pouvant être une div, ou  comme nous utilisons ci-dessous, avec une simple balise vide.
Une image, que nous définissons en tant que Composant React, afin de simplifier la syntaxe de notre code dans les prochaines étapes notemment pour définir notre affichage. Comme cette image est un composant, nous la nommons Svg en PascalCase.
Enfin, nous utilisons ici le format SVG, mais nous pouvons utiliser d'autres formats tels que le PNG, GIF et JPG. Mais dans ce cas il faudra installer le plugin officiel de gestion d' images pour les formats PNG ou JPG.

Nous appelons notre tableau featuresList car c'est la liste des fonctionnalités.

const FeatureList = [
  {
    title: "Le Lab 🧪",   
    Svg: require("../../static/img/chemistry.svg").default,
    description: (
      <>
        Le laboratoire vous permet d’héberger votre code et de collaborer dessus avec Git. C&apos;est plus qu&apos;un serveur Git puisque vous pouvez gérer un projet complet dessus. Le Lab est le cœur de Froggit et il sert aussi de base centrale pour l’authentification unique (SSO) des autres services.
      </>
    ), 
  },     
  {
    title: "La CI 🔁",  
    Svg: require("../../static/img/rainbow.svg").default,
    description: (
      <>
        Fournir un outil de collaboration de code sans Intégration Continue n&apos;était pas pensable pour nous. C&apos;est pour cela que nous avons mis en place une CI avec un serveur et un débordement élastique dans Kubernetes.
      </>
    ), 
  },
  {
    title: "Le Chat 💬",  
    Svg: require("../../static/img/selfie.svg").default,
    description: (
      <>
        Pour collaborer les équipes ont besoin de communiquer. Qu&apos;elles soient physiquement au même endroit ou non une communication asynchrone est importante. C&apos;est pour cela que Froggit inclut un chat avec le service. Vous pourrez même faire remonter vos alertes et messages venant du Lab. L&apos;authentification se fera grâce au Lab, vous n&apos;aurez qu&apos;un seul compte pour tous les services de Froggit. Vous pourrez abandonner Slack sans crainte !
      </>
    ),
  },
  {
    title: "Les Pages 🔖",  
    Svg: require("../../static/img/read-smile.svg").default,
    description: (
      <>
        Vous avez besoin de créer des sites statiques pour vos documentations. C&apos;est pour cela que nous mettons en place les GitLab pages qui vous permettent d&apos;héberger des sites statiques.
      </>
    ),
  },
];  


Notre section affichera les 4 éléments, chacune avec un titre, une description et une image en Svg.

Les paramètres d'un composant
On peut passer des paramètres au composant grâce :

au propriété de la balise ;
au texte entre les balises ouvrante et fermantes.

Pour notre exemple on va créer le compostant TextRight avec deux paramètres :

une image
un bloc de texte

On va utiliser ce composant depuis un fichier MDX.
Le fichier  TextRight.js se trouve dans le répertoire des composants :  src/components. Comme on a du CSS exclusif à ce composant on crée un fichier avec la même base de nom :TextRight.module.css. Le .module.css signifie qu'il apportera des modifications uniquement au fichier .js pourtant le même nom, et pas les autres page du site.

Le composant

// src/components/TextRight.js
import React from "react";
import styles from "./TextRight.module.css";
import useBaseUrl from '@docusaurus/useBaseUrl';

export default function TextRight({children, img}) {
  return (
    <>
      <div className="row">
        <div className="col col--6">
          <img src={useBaseUrl(img)} />
        </div>
        <div className="col col--6">
          {children}
        </div>
      </div>
    </>
  );
}


D'abord on fait ses imports : React, le module css, et un useBaseUrl.
On utilise useBaseUrl pour obtenir l'URL de base du site et pouvoir avoir renvoyer les bonnes URL au HTML sui sera généré. Voir la doc de Docusaurus.
On exporte la fonction (qui sera importée ensuite dans le fichier mdx). Cette fonction possède deux paramètres :


children, reprenant les propriétés des "parents" pour les renvoyer en sortie, c'est ce qui se trouve entre les deux balises du composant React.

img qui contient le chemin relatif de notre image depuis le répertoire static/ et qui sera passé dans les propriété de la balise ouvrante de notre composant au moment de l'appel.

La fonction génère du HTML, se qui se trouve dans return :

Des "blocks" (div) avec des className (ou des class en html).
Pour l'image on utilise le fonction useBaseUrl qui renvois l'URL finale de notre img (en paramètre).
Pour notre texte il est contenu dans le paramètre children.

Dans le dossier static/ : mettre l'image en veillant à bien choisir son nom.

L'appel du composant
Dans le dossier src/pages : on à un fichier MDX, communaute.mdx dans notre exemple.
Dans notre fichier MDX il faut :

Importer la fonction TextRight depuis le chemin du composant.
Utiliser le composant avec la nouvelle balise créé <TextRight> et </TextRight>


Comme par exemple :

import TextRight from '@site/src/components/TextRight';

<TextRight img="/img/mon_image.png">

# Mon Titre

Mon texte

</TextRight>



Attention au multiples CSS
⚠️ Remarque: veiller aux erreurs dans les fichiers custom.css et .module.css.
Nous avons constaté que si une classe reçoit des attribues dans le fichier mon_composant.module.css et le fichier custom.css alors il sera retenu en priorité ce qui se trouve dans le fichier custom.css qui est le fichier général de style.
Nous n'avons pas de réponse à la question :

Est-ce que ce qui se trouve dans le fichier mon_composant.module.css s'ajoute à ce qui se trouve dans le fichier custom.css ?


Affichage
Il faut maintenant définir l'affichage que nous souhaitons que ce composant renvoie.
Nous avons créé une fonction qui renvoie le rendu, grâce à un template.
L'utilisation du JSX simplifie cette tâche, nous avons trois parties qui sont les clés issues de notre tableau à afficher.
Tous élément JavaScript doit alors être affiché entre accolade: {---} afin d'être correctement interprété.
Notre affichage doit impérativement être entouré de balises parents, telle une div.
Nous passons comme argument les clés de notre tableau (Svg, title, description) à notre fonction

function Feature({Svg, title, description}) {
  return (
    <div className="sectionsReverse row">    
      <div className="col col--5 text--center">
        <Svg className={styles.svg} alt={title} />
      </div>
      <div className="col col--5">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>      
    </div>
    
  );
}


Ainsi, notre composant va renvoyer notre Svg en image, le titre en h3 et la description en paragraphe.

Réutilisation du composant
Nous devons également importer certains utilitaires, selon nos besoins (la librairie React, les modules CSS), puis exporter notre composant, pour qu'il puisse être réutilisé.
Il est nécessaire d'utiliser le système d'import/export de modules JavaScript.

Définition des Comportements
Nous itérons sur chaque élément de notre tableau, en utilisant la méthode JavaScript .map, qui est très souvent utilisé dans les Composants React.
La méthode map() crée un nouveau tableau avec les résultats de chaque élément du tableau. Voici un tutoriel expliquant cette méthode.

export default function HomepageFeatures() {
  return (
    <section className="sections container" id="features">      
      <h2 className={clsx("text--center text--uppercase", styles.title)}>Fonctionnalités</h2>
      <div>
        {FeatureList.map((props, idx) => (
          <Feature key={idx} {...props} />
        ))}
      </div>
    </section>
  );
}


Nous utilisons grâce à une fonction fléchée, une fonction de Callback pour boucler sur notre tableau, qui prend 2 paramètres, les props qui représentent les valeurs du tableau, et idx, les clés. les ... est un Spread Operator.
Notre Composant HomepageFeatures est également exporté au début de la déclaration de la fonction avec export default.

Utilisation du composant
Nous pouvons désormais utiliser notre composant dans un composant parent.
Dans notre cas, ce composant parent est la page d'accueil du site src/pages/index.js.
Il suffit d'importer notre composant enfant, que nous venons de créer, et de le mettre, comme une balise, dans la partie return de la fonction Home. La fonction Home retourne la vue de notre page :

import React from "react";
import HomepagePromises from "../components/HomepagePromises";
import HomepageFeatures from "../components/HomepageFeatures";

//    (...)

export default function Home() {
  return (
    <Layout>
      <HomepageHeader />
      <main>
        <HomepagePromises />
        <HomepageFeatures />
      </main>
    </Layout>
  );
}


Entre nos balises Layout, nous retrouvons 3 composants enfants, d'abord l'entête (header) nommé <HomepageHeader /> qui a été créé à l'intérieur même de ce composant.
Puis nous retrouvons 2 Composants, qui ont été importé depuis le dossier src/components/:


<HomepagePromises /> pour la section des promesses

<HomepageFeatures /> pour la section des fonctionnalités


Feuille de style CSS
Un Composant peut avoir sa propre feuille de style CSS. Nous donnons le même nom que le fichier de composant au fichier CSS suivi du suffixe .module.css.
Un composant peut également utiliser la feuille de style générale custom.css. Ce fichier regroupe les attributs de styles standard commun à tous le site.
Chaque feuille de style que l'on souhaite utiliser doit être importé dans le Composant.
    <a href="https://vitejs.dev" target="_blank">
      <img src="/vite.svg" class="logo" alt="Vite logo" />
    </a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">
      <img src="${javascriptLogo}" class="logo vanilla" alt="JavaScript logo" />
    </a>
    <h1>Hello Vite!</h1>
    <div class="card">
      <button id="counter" type="button"></button>
    </div>
  
  </div>
`

setupCounter(document.querySelector('#counter'))
